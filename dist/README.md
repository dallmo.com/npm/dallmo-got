# dallmo-got

- simple wrapper based on [got][ref-2], aimed only as shorthand functions on a very limited subset of functions provided by [got][ref-2].
- native ESM for adopting got v13 ;
- to use this inside CJS requires the [dynamic import() function][ref-3] ; 

[ref-1]: https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
[ref-2]: https://www.npmjs.com/package/got
[ref-3]: https://v8.dev/features/dynamic-import
