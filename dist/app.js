"use strict";

import got from 'got';

//////////////////////////////////////////////////
// unify the response obj for each response
async function response_to_json( response ){

  let res_json = {};
      res_json.status_code = response.statusCode;
      res_json.body = JSON.parse( response.body );

  return res_json;

}// function response_to_json
//////////////////////////////////////////////////
// replace this with alias ??
async function get( url ){
  // headers : Accept
  const response = await got( url, {
                            method: 'GET',
                            headers: {
                              'Accept': 'application/json'
                            }
                          });

  return await response_to_json( response );

}// function get
//////////////////////////////////////////////////
// update data via trello api
async function put( url, data ) {

  const data_obj = {
    value: data
  };

  // headers : Content-Type
  const response = await got( url, {
                            method: 'PUT',
                            headers: {
                              'Content-Type': 'application/json'
                            },
                            body: JSON.stringify( data_obj )
                          });

  return await response_to_json( response );

}// function put
//////////////////////////////////////////////////
// delete data
async function del( url ) {

  // headers : Accept
  const response = await got( url, {
                            method: 'DELETE',
                            headers: {
                              'Accept': 'application/json'
                            }
                          });

  return await response_to_json( response );

}// function del
//////////////////////////////////////////////////
// post data
async function post( url, data_body ) {

  // headers : Accept
  const response = await got( url, {
                            method: 'POST',
                            headers: {
                              'Accept': 'application/json'
                            },
                            body: data_body
                          });

  return await response_to_json( response );

}// function post
//////////////////////////////////////////////////
// consolidate all functions under the same object
const dallmo_got = {
  
  get,
  put,
  del,
  post,
  
}; // dallmo_got
//////////////////////////////////////////////////
// export the object which carries all functions
export {
  dallmo_got
};
