
import {dallmo_got} from 'dallmo-got';
import {dallmo_yaml} from 'dallmo-yaml';

//----------------------------------------------------
// read config from yaml file
  const config_file = "/home/jimz/.config/trello/config.yaml";
  const trello_config_obj = await dallmo_yaml( config_file );

    const api = trello_config_obj.api;
      const api_url   = api.url;
      const api_key   = api.key;
      const api_token = api.token;
        const auth_suffix = "?key=".concat( api_key, "&token=", api_token );

//----------------------------------------------------
// define runtime vars here
//----------------------------------------------------
//const board_id = trello_config_obj.board_id;
const board_id = "epwMXfVF";
const card_id = trello_config_obj.card_id.dev;
const list_id = "6160ac9b75579e728ca6e240";

let url;
    //--------------------------------------------------------------
    // get lists on a board
    //--------------------------------------------------------------
    url = api_url.concat( "/boards/", board_id, "/lists", auth_suffix, "&fields=name" );
    const response = await dallmo_got.get( url );

    console.log("----------------");
    console.log( response );
    console.log( response.status_code );

