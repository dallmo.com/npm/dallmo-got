# dallmo-got

- simple wrapper based on [got][ref-2], aimed only as shorthand functions on a very limited subset of functions provided by [got][ref-2].
- compatible with both cjs and esm, by following [the advice of a post by Dan Fabulich][ref-1]

[ref-1]: https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
[ref-2]: https://www.npmjs.com/package/got
