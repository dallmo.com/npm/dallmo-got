
const dallmo_got = require( 'dallmo-got' );
const read_config = require('dallmo-read-config');

//----------------------------------------------------
// read config from yaml file
  const config_file = "/home/jimz/.config/trello/config.yaml";
  read_config( config_file ).then( trello_config_obj => {


    const api = trello_config_obj.api;
      const api_url   = api.url;
      const api_key   = api.key;
      const api_token = api.token;
        const auth_suffix = "?key=".concat( api_key, "&token=", api_token );

//----------------------------------------------------
// define runtime vars here
//----------------------------------------------------
//const board_id = trello_config_obj.board_id;
const board_id = "epwMXfVF";
const card_id = trello_config_obj.card_id.dev;
const list_id = "6160ac9b75579e728ca6e240";

let url;
    //--------------------------------------------------------------
    // get lists on a board
    //--------------------------------------------------------------
    url = api_url.concat( "/boards/", board_id, "/lists", auth_suffix, "&fields=name" );
    dallmo_got.get( url ).then( response => {
      console.log("----------------");
      console.log( response );
      console.log( response.status_code );
    });//dallmo_got promise

  });//read_config promise

